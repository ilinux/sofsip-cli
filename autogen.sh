#!/bin/sh 

autoreconf -i
find . \( -name 'run*' -o -name '*.sh' \) -a -type f | xargs chmod +x
